﻿using NetAutoGUI;

namespace MyNamespace;

public class 应用程序
{
    public void MM()
    {

        // 判断应用程序是否正在运行
        if (GUI.Application.IsApplicationRunning("WeChat"))
        {
            Console.WriteLine("已经运行");
        }
        else
        {
            Console.WriteLine("不在运行");
        }

        // 根据窗口标题精确查找窗口
        //Window? win = GUI.Application.FindWindowByTitle("记事本");
        // 根据窗口标题+通配符（*）模糊查找窗口
        Window? win = GUI.Application.FindWindowLikeTitle("*记事本*");
        if (win != null)
        {
            // 根据进程名字结束进程
            GUI.Application.KillProcesses("notepad");
            Console.WriteLine("找到记事本了");
        }
        else
        {
            // 打开一个进程
            GUI.Application.LaunchApplication("notepad");
            Console.WriteLine("没找到记事本");
        }

        GUI.Application.KillProcesses("notepad++.exe");
        // 打开一个进程
        GUI.Application.LaunchApplication("F:\\Notepad++\\notepad++.exe");
        // 等待一个进程启动完成
        Window win1 = GUI.Application.WaitForWindowLikeTitle("*新文件*", 0.1);
        if (win1 != null)
        {
            Console.WriteLine("找到了");
        }
        else
        {
            Console.WriteLine("没找到");
        }
    }
}